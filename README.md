# GSoC 2024

* [Differentiable Logic for Interactive Systems](https://forum.beagleboard.org/t/weekly-progress-report-differentiable-logic-for-interactive-systems-and-generative-music/38486)
* [RISC-V Core for Low-Latency I/O](https://forum.beagleboard.org/t/weekly-progress-report-low-latency-i-o-risc-v-cpu-core-in-fpga-fabric/38488)
* [Zephyr on BeagleBone AI-64 R5](https://forum.beagleboard.org/t/weekly-progress-report-thread-upstreaming-zephyr-support-on-beaglebone-ai-64/38509)
* [AI-based Commercial Detection and Replacement](https://forum.beagleboard.org/t/enhanced-media-experience-with-ai-powered-commercial-detection-and-replacement/37358)